package com.example.demo.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MemberService {

    private final MemberRepository memberRepository;

    @Autowired
    public MemberService(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    public Member getMemberById(int id) {
        Optional<Member> byId = memberRepository.findById((long) id);
        if (byId.isPresent()) {
            return byId.get();
        } else {
            throw new IllegalStateException("data not found");
        }
    }

    public List<Member> getMembers() {
        return memberRepository.findAll();
    }

    public void addNewMember(Member member) {
        Optional<Member> memberByNameOrEmail = memberRepository.findMemberByNameOrEmail(member.getName(), member.getEmail());
        if (memberByNameOrEmail.isPresent()) {
            throw new IllegalStateException("Name or Email already exist");
        }
        System.out.println(member.getDateOfBirth() + member.getMobileNo());
        memberRepository.save(member);
    }

    public void updateMember(Member member) {
        Optional<Member> byId = memberRepository.findById((long) member.getId());
        if (byId.isEmpty()) {
            throw new IllegalStateException("Id not found");
        }
        memberRepository.updateById(member.getId(),
                member.getName(),
                member.getDateOfBirth(),
                member.getEducation(),
                member.getEmail(),
                member.getMobileNo());
    }

    public void deleteMember(int id) {
        Optional<Member> byId = memberRepository.findById((long) id);
        if (byId.isEmpty()) {
            throw new IllegalStateException("Id not found");
        }
        memberRepository.deleteById((long) id);
    }
}
