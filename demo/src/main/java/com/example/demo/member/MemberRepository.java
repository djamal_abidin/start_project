package com.example.demo.member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Transactional
public interface MemberRepository
        extends JpaRepository<Member, Long> {

    @Query("SELECT s FROM Member s WHERE s.name = ?1 OR s.email = ?2")
    Optional<Member> findMemberByNameOrEmail(String name, String email);

    @Modifying
    @Query("UPDATE Member s SET name = ?2, dateOfBirth = ?3, education = ?4, email = ?5, mobileNo = ?6 WHERE s.id = ?1")
    void updateById(
            int id,
            String name,
            Date dob,
            String education,
            String email,
            String mobileNo
    );
}
