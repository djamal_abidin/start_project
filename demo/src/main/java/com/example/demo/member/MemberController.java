package com.example.demo.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/member")
public class MemberController {

    private final MemberService memberService;

    @Autowired
    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping
    public List<Member> getMembers() {
        return memberService.getMembers();
    }

    @PostMapping
    public void registerMember(@RequestBody Member member){
        memberService.addNewMember(member);
    }

    @PutMapping
    public void updateMember(@RequestBody Member member){
        memberService.updateMember(member);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteMember(@PathVariable("id") int id){
        memberService.deleteMember(id);
    }
}
