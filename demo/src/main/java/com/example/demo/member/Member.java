package com.example.demo.member;

import jakarta.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table
public class Member {

    @Id
    @SequenceGenerator(
            name = "member_sequence",
            sequenceName = "member_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
            generator = "member_sequence"
    )
    private int id;
    @Column(length = 20)
    private String name;
    @Column(name = "date_of_birth")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateOfBirth;
    @Column(columnDefinition = "TEXT")
    private String education;
    @Column(length = 40)
    private String email;
    @Column(name = "mobile_no", length = 20)
    private String mobileNo;

    public Member() {
    }

    public Member(String name, Date dateOfBirth, String education, String email, String mobileNo) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.education = education;
        this.email = email;
        this.mobileNo = mobileNo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getEducation() {
        return education;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", education='" + education + '\'' +
                ", email='" + email + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                '}';
    }
}
