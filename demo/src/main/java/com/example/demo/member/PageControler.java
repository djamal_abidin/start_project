package com.example.demo.member;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

@Controller
public class PageControler {

    private final MemberService memberService;

    public PageControler(MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping(path = "member")
    public String getMembers(Model model){
        List<Member> members = memberService.getMembers();
        model.addAttribute("members", members);
        return "listPage";
    }

    @GetMapping(path = "/member/detail/{id}")
    public String getMember(@PathVariable("id") int id,  Model model){
        Member memberById = memberService.getMemberById(id);
        model.addAttribute("member", memberById);
        return "detail";
    }

    @GetMapping(path = "/member/delete/{id}")
    public String deleteMember(@PathVariable("id") int id,  Model model){
        memberService.deleteMember(id);
        return "redirect:/member";
    }

    @PostMapping(path = "/save-member")
    public String saveMember(
            @ModelAttribute("name") String name,
            @ModelAttribute("dateOfBirth") String dob,
            @ModelAttribute("education") String education,
            @ModelAttribute("email") String email,
            @ModelAttribute("mobileNo") String phone
    ) throws ParseException {
        Member member = new Member(
                name,
                new SimpleDateFormat("yyyy-MM-dd").parse(dob),
                education,
                email,
                phone
        );
        System.out.println("=> " + member);
        memberService.addNewMember(member);
        return "redirect:/member";
    }

    @GetMapping(path = "/add-member")
    public String addMember(Model model){
        model.addAttribute("member", new Member());
        return "addForm";
    }

    @PostMapping(path = "/update-member/{id}")
    public String updateMember(
            @PathVariable("id") int id,
            @ModelAttribute("name") String name,
            @ModelAttribute("dateOfBirth") String dob,
            @ModelAttribute("education") String education,
            @ModelAttribute("email") String email,
            @ModelAttribute("mobileNo") String phone
    ) throws ParseException {
        Member member = new Member(
                name,
                new SimpleDateFormat("yyyy-MM-dd").parse(dob),
                education,
                email,
                phone
        );
        member.setId(id);
        System.out.println("id => " + member);
        memberService.updateMember(member);
        return "redirect:/member";
    }

    @GetMapping(path = "/edit-member/{id}")
    public String editMember(@PathVariable("id") int id, Model model){
        Member memberById = memberService.getMemberById(id);
        model.addAttribute("member", memberById);
        return "editForm";
    }
}
