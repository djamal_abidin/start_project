package com.example.demo.member;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

@Configuration
public class MemberConfig implements WebMvcConfigurer {

    @Bean
    CommandLineRunner commandLineRunner(MemberRepository repository) {
        return args -> {
            Member budi = new Member(
                    "Budi",
                    new GregorianCalendar(1980, 10, 17).getTime(),
                    "S1 Universitas Pajajaran",
                    "budi@gmail.com",
                    "087712341234"
            );

            Member andi = new Member(
                    "Andi",
                    new GregorianCalendar(1982, 9, 20).getTime(),
                    "S1 Universitas Pakuan",
                    "andi@gmail.com",
                    "087712121212"
            );

            Member susi = new Member(
                    "Susi",
                    new GregorianCalendar(1981, 1, 10).getTime(),
                    "S1 Institut Pertanian Bogor",
                    "susi@gmail.com",
                    "085612211221"
            );

            Member tuti = new Member(
                    "Tuti",
                    new GregorianCalendar(1981, 5, 12).getTime(),
                    "S1 Universitas Ibnu Khaldun",
                    "tuti@gmail.com",
                    "083834123412"
            );

            repository.saveAll(
                    List.of(budi, andi, susi, tuti)
            );
        };
    }

    @Bean
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("en", "US"));
        return localeResolver;
    }

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

}
